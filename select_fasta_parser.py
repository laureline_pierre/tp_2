# -*- coding: utf-8 -*-

import argparse
import sys
from adn import is_valid


def create_parser():
    """ Declares new parser and adds parser arguments """
    program_description = ''' reading fasta file and checking sequence format '''
    parser = argparse.ArgumentParser(
        add_help=True, description=program_description)
    parser.add_argument('-i', '--inputfile', default=sys.stdin,
                        help="required input file in fasta format", type=str, required=True)
    return parser


def main():
    """ Main function for reading fasta file and checking sequence format """
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    with open(args["inputfile"], 'r') as f:
        if not (args["inputfile"].endswith(".fasta")):
            print("Le fichier n'est pas au format fasta")
        seq_dict = {}
        seq_id = ''
        for line in f:
            if line.startswith(">"):
                seq_id = line[1:].split()[0]
                seq_dict[seq_id] = ""
            else:
                seq_dict[seq_id] += line.strip()
        if bool(seq_dict) == False:
            print("Your file is empty ")
        else:
            pass
        for key, value in seq_dict.items():
            if is_valid(value) is False:
                print("The following sequence is not correct : ", key)
            else:
                print("The following sequence is correct : ", key)


if __name__ == "__main__":
    main()
