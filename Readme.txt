TP2 - INLO

Author : 

Lauréline Pierre  

Here the homework :

Utiliser le fichier adn.py fait la semaine derniere et le fichier select_fasta_parser.py 
pour faire un parser de fichier fasta qui doit vérifier que le fasta contenu est bien sous forme de
nucléotides. 
Un exemple de fichier fasta vous ait fourni sous forme d'un fichier : exemple.fasta

Votre script doit indiquez pour CHAQUE sequence si la sequence est valide ou non
Facultafif :
indiquez la longueur des sequences FASTA pour chaque sequences
indiquez la position ou une lettre de la sequence n'est pas un nucléotide connu

GOAL :

Verify is a fasta file is in the good format and if its sequence is valid

HOW to run the file : 

use the following line with the name of your file : python3 select_fasta_parser.py -i file_name
